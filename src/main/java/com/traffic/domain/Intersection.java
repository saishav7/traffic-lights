package com.traffic.domain;

import lombok.Data;

/**
 * Created by saishav on 9/03/2016.
 */

@Data
public class Intersection {

    public Intersection() {

    }

    public Intersection(Intersection intersection) {
       this.eastWest = intersection.getEastWest();
        this.northSouth = intersection.getNorthSouth();
    }

    private Light eastWest;
    private Light northSouth;
}
