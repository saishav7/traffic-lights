package com.traffic.domain;

import lombok.Data;

/**
 * Created by saishav on 9/03/2016.
 */

@Data
public class Light {

    private String direction;
    private String color;

    private long lastupdate;

}
