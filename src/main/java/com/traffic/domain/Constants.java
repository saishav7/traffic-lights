package com.traffic.domain;

/**
 * Created by saishav on 9/03/2016.
 */
public class Constants {

    public static final String COLOR_RED = "Red";
    public static final String COLOR_YELLOW = "Yellow";
    public static final String COLOR_GREEN = "Green";

    public static final String DIRECTION_NS = "North - South";
    public static final String DIRECTION_SN = "South - North";
    public static final String DIRECTION_EW = "East - West";
    public static final String DIRECTION_WE = "West - East";
}
