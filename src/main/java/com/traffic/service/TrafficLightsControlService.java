package com.traffic.service;

import com.traffic.domain.Constants;
import com.traffic.domain.Intersection;
import com.traffic.domain.Light;

import java.util.Date;

/**
 * Created by saishav on 9/03/2016.
 */
public class TrafficLightsControlService implements Runnable {


    public static final long FIVE_MINUTES = 5 * 60 * 1000;
    public static final long THIRTY_SECONDS = 30 * 1000;
    public static final long TEN_SECONDS = 10 * 1000;

    public Intersection intersection;

    public void init() {

        Light ewLight = new Light();
        ewLight.setDirection(Constants.DIRECTION_EW);
        ewLight.setColor(Constants.COLOR_GREEN);
        ewLight.setLastupdate(System.currentTimeMillis());

        Light nsLight = new Light();
        nsLight.setDirection(Constants.DIRECTION_NS);
        nsLight.setColor(Constants.COLOR_RED);
        nsLight.setLastupdate(System.currentTimeMillis());

        intersection = new Intersection();
        intersection.setEastWest(ewLight);
        intersection.setNorthSouth(nsLight);

        printStatusAtIntersection(intersection);

    }

    public static void main(String args[]) {
        (new Thread(new TrafficLightsControlService())).start();
    }

    public void run() {

        init();
        long lastUpdated = System.currentTimeMillis();

        while (true) {

            if(System.currentTimeMillis() > lastUpdated + TEN_SECONDS) {
                //switch lights
                Intersection newIntersection = new Intersection();

                Light newEwLight = getNextColor(intersection.getEastWest());
                Light newNsLight = getNextColor(intersection.getNorthSouth());

                intersection.setEastWest(newEwLight);
                intersection.setNorthSouth(newNsLight);
                printStatusAtIntersection(intersection);

                lastUpdated = System.currentTimeMillis();
            }
        }
    }

    private void printStatusAtIntersection(Intersection intersection) {

        System.out.println("Direction NS has " + intersection.getNorthSouth().getColor() + " at " + new Date());
        System.out.println("Direction EW has " + intersection.getEastWest().getColor() + " at " + new Date());
    }


    public Light getNextColor(Light light) {

        long now = System.currentTimeMillis();
        if(light.getColor().equals(Constants.COLOR_RED)) {
            if(now > light.getLastupdate() + FIVE_MINUTES) {
                light.setColor(Constants.COLOR_GREEN);
                light.setLastupdate(now);
            }
        } else if(light.getColor().equals(Constants.COLOR_GREEN)) {
            if(now > light.getLastupdate() + FIVE_MINUTES) {
                light.setColor(Constants.COLOR_YELLOW);
                light.setLastupdate(now);
            }
        } else if(light.getColor().equals(Constants.COLOR_YELLOW)) {
            if(now  > light.getLastupdate() + THIRTY_SECONDS) {
                light.setColor(Constants.COLOR_RED);
                light.setLastupdate(now);
            }
        }
        return light;
    }

}
